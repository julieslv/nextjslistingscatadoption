// var faker = require("faker");
var faker = require('faker/locale/en_GB');
var _ = require('lodash');
// faker.locale = "en_GB";
module.exports = () => {
  const tagArray = () => {
    const words = faker.random.words(4);
    return words.split(/\s+/);
  };

  return {
    listings: _.times(10, (n) => ({
      id: n,
      entryDate: faker.date.recent(),
      newAdoption: faker.random.boolean(),
      guidePrice: faker.random.number({
        min: 60000,
        max: 1000000,
      }),
      name: faker.name.firstName(),
      disposition: {
        chipped: faker.random.boolean(),
        age: faker.random.number({
          min: 1,
          max: 26,
        }),
        medication: faker.random.boolean(),
        neutered: faker.random.boolean(),
        onlyKitty: faker.random.boolean(),
      },
      summary: faker.random.words(5),
      description: faker.lorem.sentence(35),
      imageData: {
        imageMd: `cat${n}.jpg`,
        imageLg: `cat${n}.jpg`,
        imageSm: `cat${n}.jpg`,
        imageDescription: faker.random.words(4),
      },
      statusExpired: faker.random.boolean(),
      tags: tagArray(),
    })),
  };
};
