# Generating fake data

## Requirements

### Yarn or npm

* [json-server](http://localhost:3003/listings)
* [faker](https://github.com/Marak/faker.js)
* lodash

## run the server

in _db-server_ folder,
run
`json-server generate.js --port 3003`

## to amend the structure

update `generate.js` and follow the instructions from the faker github account

for a quick [tutorial](https://egghead.io/lessons/javascript-creating-demo-apis-with-json-server) visit


The [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client) VS Code extension is also useful
