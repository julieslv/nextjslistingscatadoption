{
  "root": true,
  "parser": "@typescript-eslint/parser",
  "plugins": [
    "react",
    "react-hooks",
    "eslint-plugin-jsx-a11y",
    "@typescript-eslint"
  ],
  "extends": [
    // "eslint:recommended",
    "plugin:react/recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    // "prettier",
    "prettier/@typescript-eslint",
    // "prettier/babel",
    // "prettier/react"
    "plugin:prettier/recommended" // must be last
  ],
  "parserOptions":  {
    "ecmaVersion":  2018,  // Allows for the parsing of modern ECMAScript features
    "sourceType":  "module",  // Allows for the use of imports
    "ecmaFeatures":  {
      "jsx":  true  // Allows for the parsing of JSX
    }
  },
  "rules":{
    "no-debugger": 0,
    "no-alert": 0,
    "no-await-in-loop": 0,
    "max-len": 0,
    "prefer-promise-reject-errors": ["off"],
    "no-return-assign": ["error", "except-parens"],
    "no-restricted-syntax": [2, "ForInStatement", "LabeledStatement", "WithStatement"],
    "no-unused-vars": ["error", { "vars": "all", "args": "after-used", "ignoreRestSiblings": false }],
    "prefer-const": [
      "error",
      {
        "destructuring": "all"
      }
    ],
    "arrow-body-style": [2, "as-needed"],
    "no-unused-expressions": [
      2,
      {
        "allowTaggedTemplates": true
      }
    ],
    "no-param-reassign": [
      2,
      {
        "props": false
      }
    ],
    "no-console": 1,
    "import/prefer-default-export": 0,
    "import": 0,
    "func-names": 0,
    "space-before-function-paren": 2,
    "comma-dangle": 0,
    "import/extensions": 0,
    "no-underscore-dangle": 0,
    "consistent-return": 0,
    "arrow-parens": 0,
    "padded-blocks": 0,
    "prefer-template": 1,
    "no-else-return": 1,
    "no-nested-ternary": 0,
    "no-shadow": [
      2,
      {
        "hoist": "all",
        "allow": ["resolve", "reject", "done", "next", "err", "error"]
      }
    ],
    "quotes": [
      2,
      "single",
      {
        "avoidEscape": true,
        "allowTemplateLiterals": true
      }
    ],
    "radix": 0,
    "react/display-name": 1,
    "react/no-array-index-key": 0,
    "react/react-in-jsx-scope": 0,
    "react/prefer-stateless-function": 0,
    "react/forbid-prop-types": 0,
    "react/no-unescaped-entities": 0,
    "react/jsx-filename-extension": ["off"],
    "react/prop-types": 0,
    "react/require-default-props": 0,
    "@typescript-eslint/explicit-function-return-type": 0,
    "@typescript-eslint/explicit-member-accessibility": 0,
    "@typescript-eslint/indent": 0,
    "@typescript-eslint/member-delimiter-style": 0,
    "@typescript-eslint/no-explicit-any": 0,
    "@typescript-eslint/no-var-requires": 0,
    "@typescript-eslint/no-use-before-define": 0,
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "warn",
    "prettier/prettier": [
      "error",
      {
        "trailingComma": "es5",
        "singleQuote": true,
        "printWidth": 180
      }
    ],
    "jsx-a11y/accessible-emoji": 1,
    "jsx-a11y/href-no-hash": "off",
    "jsx-a11y/anchor-is-valid": [
      "warn",
      {
        "aspects": ["invalidHref"]
      }
    ]
  },
  "settings":  {
    "react":  {
      "version":  "detect"  // Tells eslint-plugin-react to automatically detect the version of React to use
    }
  }
}
