import React from 'react';
import { storiesOf } from '@storybook/react';
import Button from './button';

// src/stories/Button.stories.tsx

storiesOf('Button', module).add('Primary Button', () => {
  return (
    <Button btnType="primary" size="small">
      Button Primary
    </Button>
  );
});
