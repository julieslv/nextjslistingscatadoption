import React, {Fragment } from 'react';

import styled from 'styled-components';

const Button = styled.button`
  background-color: #253748;
  color: #ffffff;
  font-weight: 700;
  border: 0 none;
  border-radius: 4px;
  padding: 0.2rem 0.5rem;
`;

type ButtonProps = {
  btnType: string,
  size: string,
  children: string
};

export default ({ btnType, size, children }: ButtonProps) => {
  return (
    <Fragment>
      {/* <Button btnType={btnType} size={size}>{children}</Button> */}
      <Button>{children}</Button>
    </Fragment>
  )
}

