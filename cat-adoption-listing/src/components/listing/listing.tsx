import styled from 'styled-components';
import Link from 'next/link';
import Image from '@/components/image/image';
import Tag from '@/components/tag/tag';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSmileBeam, faSyringe, faMicrochip, faCut, faBirthdayCake } from '@fortawesome/free-solid-svg-icons';

interface ListProps {
  detailsList?: DetailsListInterface[] | undefined;
  tagType: string | [];
  text: string;
  srcImages: string;
}

interface DetailsListInterface {
  id: number;
  entryDate: string;
  name: string;
  disposition: {
    chipped: boolean;
    age: number;
    medication: boolean;
    neutered: boolean;
    onlyKitty: boolean;
  };
  summary: string;
  description: string;
  imageData: {
    imageMd: string;
    imageLg: string;
    imageDescription: string;
  };
  statusExpired: boolean;
  [key: string]: any;
}

const List: React.FunctionComponent<DetailsListInterface> = ({ detailsList }: ListProps) => (
  <ListEl>
    {detailsList?.map((listItem, index) => (
      <ListItem key={index}>
        <div className="picture">
          {listItem.newAdoption === true && <Tag tagType="image" tagText="New Adoption" />}
          <Image srcImages={listItem.imageData} />
        </div>
        <div className="details">
          <Name>
            <span className="title">Name</span>
            <h2>{listItem.name}</h2>
          </Name>
          <div className="info">
            <div>
              <span className="icon-wrap">
                <FontAwesomeIcon icon={faBirthdayCake} />
              </span>
              {listItem.disposition.age}
            </div>
            <div>
              <span className="icon-wrap">
                <FontAwesomeIcon icon={faMicrochip} />
              </span>
              {listItem.disposition.chipped ? 'yes' : 'no'}
            </div>
            <div>
              <span className="icon-wrap">
                <FontAwesomeIcon icon={faSyringe} />
              </span>
              {listItem.disposition.medication ? 'yes' : 'no'}
            </div>
            <div>
              <span className="icon-wrap">
                <FontAwesomeIcon icon={faCut} />
              </span>
              {listItem.disposition.neutered ? 'yes' : 'no'}
            </div>
            <div>
              <span className="icon-wrap">
                <FontAwesomeIcon icon={faSmileBeam} />
              </span>
              {listItem.disposition.onlyKitty ? 'yes' : 'no'}
            </div>
          </div>
          <h3>{listItem.summary}</h3>
          <p>{listItem.description}</p>
          <div>{listItem.statusExpired}</div>
        </div>
        <div className="footer">
          <Link href="/listings">
            <a>Full info and adoption process</a>
          </Link>
        </div>
      </ListItem>
    ))}
  </ListEl>
);

/* Styles */
const Name = styled.div`
  font-size: 1.5rem;
  color: #005064;
  margin-bottom: 0.785rem;
  h2 {
    margin: 0;
    line-height: 1;
  }
  .title {
    color: #0091b6;
    font-size: 0.5em;
    display: block;
  }
`;
const ListEl = styled.ul`
  margin: 0;
  padding: 0;
`;
const ListItem = styled.li`
  background-color: #ffffff;
  border: 1px solid #d7d7d7;
  border-radius: 3px;
  list-style: none;
  margin-bottom: 1.5rem;
  @media (min-width: 768px) {
    display: flex;
    flex-wrap: wrap;
  }
  h3 {
    font-size: 1.87rem;
  }
  .details,
  .picture {
    padding: 1rem;
    @media (min-width: 768px) {
      width: 50%;
    }
  }
  .picture {
    position: relative;
  }
  .info {
    padding: 0.875rem 0;
    margin: 0.24rem 0;
    display: flex;
    align-items: center;
    font-size: 0.765rem;
    font-weight: 600;
    div {
      padding: 5px;
      display: inline-flex;
      align-items: center;
    }
    .icon-wrap {
      background: #f5f5f5;
      display: inline-block;
      margin-right: 0.25rem;
      width: 2em;
      height: 2em;
      border: 1px solid rgba(0, 145, 182, 0.45);
      border-radius: 50%;
      svg {
        position: relative;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        color: #ffd000;
        path {
          stroke: 1px solid rgba(0, 145, 182, 0.45);
        }
      }
    }
  }
  .footer {
    width: 100%;
    border-top: 2px solid #0091b6;
    flex-basis: 100%;
    display: flex;
    justify-content: flex-end;
    padding: 1rem;
    a {
      background: #ffd000;
      border: 1px solid #ffd000;
      color: #005064;
      font-weight: 600;
      padding: 0.5rem 1rem;
      text-decoration: none;
      cursor: pointer;
      &:hover,
      &:active,
      &:focus {
        background-color: #ffe500;
        border-color: #ffe500;
        color: #0091b6;
      }
    }
  }
`;

export default List;
