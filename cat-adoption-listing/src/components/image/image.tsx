import styled from 'styled-components';

interface SrcImages {
  srcImages: {
    imageMd: string;
    imageLg: string;
    imageDescription: string;
  };
}
const Image = ({ srcImages }: SrcImages) => {
  const { imageMd, imageLg, imageDescription } = srcImages;
  const srcSet1 = `/images/${imageLg}  1024w, /images/${imageMd} 640w,`;
  const srcSet2 = `/images/${imageLg} 2x, /images/${imageMd} 1x`;
  return (
    <PictureEl>
      <source media="(min-width: 768px)" srcSet={srcSet1} />
      <source srcSet={srcSet2} />
      <img src={`/images/${imageMd}`} alt={imageDescription} />
    </PictureEl>
  );
};

const PictureEl = styled.picture`
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;

export default Image;
