import styled, { css } from 'styled-components';
interface IProps {
  tagType: string;
  tagText: string | [];
}

const Tag: React.Component<IProps> = ({ tagType, tagText }: IProps) => (
  <TagEl className="tag" tagType={tagType}>
    {tagText}
  </TagEl>
);

interface SCProps {
  tagType: string;
}

const TagEl = styled.div<SCProps>`
  font-weight: 700;
  text-align: center;
  padding: 0.125rem;
  display: inline-block;
  border-radius: 4px;
  padding: 0.25rem 0.5rem;
  ${({ tagType }) =>
    tagType === 'image' &&
    css`
      background-color: #00242d;
      color: #ffffff;
      position: absolute;
      top: 1.5rem;
      left: 1.5rem;
    `};
  ${({ tagType }) =>
    tagType === 'cloud' &&
    css`
      background-color: #efeff5;
      border: 1px solid #d7d7eb;
      margin-bottom: 0.875rem;
      margin-right: 0.875rem;
    `};
`;
export default Tag;
