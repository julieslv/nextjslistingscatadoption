import styled from 'styled-components';

interface IProps {
  expired: boolean;
  index: number;
}

interface checkInterface {
  checked: boolean;
  id: string;
  for: string;
}

export default ({ expired, index }: IProps) => {
  // const [expire, setExpire] = useState(initialState);

  // const onChange = event => setText(event.target.value);

  const handleChange = () => {
    console.log('clickity click the good click');
  };
  return (
    <Form>
      <input id={`checkbox${index}`} type="checkbox" checked={expired} onChange={handleChange} />
      <label htmlFor={`checkbox${index}`}>
        {expired === true && <span>Expired</span>}
        {expired === false && <span>Active</span>}
      </label>
    </Form>
  );
};

const Form = styled.form`
  input[type='checkbox'] {
    position: absolute;
    opacity: 0;
  }
  label {
    position: relative;
    display: inline-block;
    padding: 0 0 0 3em;
    height: 2em;
    line-height: 2;
    cursor: pointer;
    &:after,
    &:after {
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      left: 4px;
      display: block;
    }
    &:before {
      content: '';
      border-radius: 20%;
    }
  }

  input[type='checkbox'] + label:after {
    content: '✔';
    color: #0091b6
    line-height: 1.5;
    text-align: center;
    font-size: 1.5rem;
  }
  input[type='checkbox']:not(:checked) + label:before,
  input[type='checkbox']:checked + label:before {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 2.25em;
    height: 2.25em;
    border: 2px solid #d7d7eb;
    background-color: #efeff5;
    border-radius: 3px;
  }
  input[type='checkbox']:checked + label:before {
    /* border-color: #0091b6 */
    /* background-color:#0091b6 */
  }
  input[type='checkbox'] + label:after {
    /* transform-origin: center center; */
    opacity: 0;
    /* transform: scale(0) translateY(0); */
  }

  input[type='checkbox']:checked + label:after {
    opacity: 1;
    /* transform: scale(1) translateY(-50%); */
  }
  label:before,
  label:after {
    transition: opacity 0.25s ease;
  }
`;
