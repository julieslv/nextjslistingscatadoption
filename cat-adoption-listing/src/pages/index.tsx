import React, { Fragment } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import styled from 'styled-components';
import Image from '../assets/images/backsplash.jpg';

export const Home = (): JSX.Element => (
  <Fragment>
    <Head>
      <title>Cat adoptions listings</title>
      <meta property="og:title" content="" />
      <meta property="og:type" content="Cat adoptions listings" />
      <meta property="og:url" content="/" />
      <meta property="og:image" content="/logo192.png" />

      <meta name="viewport" content="initial-scale=1.0, width=device-width" />

      <link rel="icon" href="/favicon.ico" />
      <link rel="apple-touch-icon" href="/logo192.png" />
      <link rel="manifest" href="/manifest.json" />
      <link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@200;300;400;600&display=swap" rel="stylesheet" />
    </Head>
    <HomePageWrap>
      <img src={Image} alt="building scape" />
      <main className="container">
        <h1>Cat Adoptions</h1>
        <h2>
          <Link href="/listings">
            <a>See listings</a>
          </Link>
        </h2>
      </main>
    </HomePageWrap>
  </Fragment>
);

const HomePageWrap = styled.div`
  /* position: relative; */
  main {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100vh;
  }
  img {
    position: fixed;
    object-fit: cover;
    width: 100%;
    height: 100%;
    z-index: -1;
  }
  h1 {
    font-size: 2rem;
    color: rgba(0, 145, 182, 0.45);
    @media only screen and (min-width: 768px) {
      font-size: 3rem;
    }
    @media only screen and (min-width: 1024px) {
      font-size: 5rem;
    }
    background-color: rgba(255, 255, 255, 0.5);
    @supports (-webkit-text-stroke: 1px rgba(0, 145, 182, 0.45)) {
      -webkit-text-stroke: 1px rgba(0, 145, 182, 1);
      -webkit-text-fill-color: rgba(0, 145, 182, 1);
    }
    padding: 1rem;
  }
  h2 {
    font-size: 1rem;
    margin: 0;
    a {
      background: #ffd000;
      border: 1px solid #ffd000;
      color: #005064;
      display: inline-block;
      font-weight: 600;
      padding: 0.5rem 1rem;
      cursor: pointer;
      text-decoration: none;
      &:hover,
      &:active,
      &:focus {
        background-color: #ffe500;
        border-color: #ffe500;
        color: #0091b6;
      }
    }
  }
`;
export default Home;
