import { Fragment } from 'react';
import styled from 'styled-components';
import { GetStaticProps } from 'next';
import Link from 'next/link';
import Head from 'next/head';
import Listing from '@/components/listing/listing';

interface IProps {
  detailsList: Array<JSON>;
}

const ListPage = ({ detailsList }: IProps): React.ReactNode => (
  <Fragment>
    <Head>
      <title>Adoption Listing</title>
      <meta property="og:title" content="" />
      <meta property="og:type" content="Adoption property Listing" />
      <meta property="og:url" content="/" />
      <meta property="og:image" content="/logo192.png" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />

      <link rel="icon" href="/favicon.ico" />
      <link rel="apple-touch-icon" href="/logo192.png" />
      <link rel="manifest" href="/manifest.json" />
    </Head>
    <ListPageWrap className="container">
      <h2>
        <Link href="/">
          <a>Back to home page</a>
        </Link>
      </h2>

      <h1>Listings page</h1>
      <Listing detailsList={detailsList} />
    </ListPageWrap>
  </Fragment>
);
export const getStaticProps: GetStaticProps = async () => {
  const response = await fetch('http://localhost:3003/listings');
  const detailsList = await response.json();
  return {
    props: {
      detailsList,
    },
  };
};

const ListPageWrap = styled.main`
  a,
  a:visited,
  a:active,
  a:hover {
    color: #0091b6;
  }
`;

export default ListPage;
