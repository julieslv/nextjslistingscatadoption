This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

Install all packages

```bash
npm install
# or
yarn
```

To run on development mode

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

This command will also generate and run the local mock server on [http://localhost:3003](http://localhost:3003).


## Learn More
### NextJS

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

### Typescript
This project uses TypeScript to help simplify code, making it easier to read and debug.

Documentation: [Typescript](https://www.typescriptlang.org/docs/home.html)

Example: [NextJS Example implementation](https://github.com/vercel/next.js/tree/master/examples/with-typescript-eslint-jest)

### FakerJS and the Mock server
JSON is generated with the aid of [FakerJS](https://github.com/marak/Faker.js/). You will find the nescessary files for generating JSON in the folder  📁_db-server_. More in the folder's _README.md_


### Styled Components
[Styled Components](https://styled-components.com/)

### Storybook
We use storybook for our design system. Please see [Writing Stories](https://storybook.js.org/docs/basics/writing-stories/) along with the [docs addon](https://www.npmjs.com/package/@storybook/addon-docs). Have a looks at the article [Storybook Docs for new frameworks](https://medium.com/storybookjs/storybook-docs-for-new-frameworks-b1f6090ee0ea). Extendibility for types and prop tables.

## Next image loader and Next image element
Solving a way to import images for components without having to server them via the public folder.

Documentation: [Next Image Element](https://github.com/jagaapple/next-image-element)

Dependency: [react-image-element-loader](https://github.com/jagaapple/react-image-element-loader)

1. Images from _lorempixel_ take forever to load... I am making the assumption that in a real world situation where images are served off the CDN and cached, this would not happen.

## Deploy on Heroku

🌳 Git-subtree 🌳
Make sure to run this from the top level of the tree:
```
git subtree push --prefix path/to/subdirectory heroku master
```
ref: [https://medium.com/@shalandy/deploy-git-subdirectory-to-heroku-ea05e95fce1f](https://medium.com/@shalandy/deploy-git-subdirectory-to-heroku-ea05e95fce1f)

## Deploy on Vercel -  not currently used.
(_default nextJs recommendation_)

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/import?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.

